import Vue from 'vue'
import VueResource from 'vue-resource'
import VueRouter from 'vue-router'
import BootstrapVue from 'bootstrap-vue'
import ModalPlugin from 'bootstrap-vue'
import LayoutPlugin from 'bootstrap-vue'
import NProgress from 'nprogress'

import AdminNavbar from './components/admin-navbar'

import {routes} from './routes.js'

import './main.scss'

Vue.use(VueResource);
Vue.use(VueRouter);
Vue.use(BootstrapVue);
Vue.use(ModalPlugin);
Vue.use(LayoutPlugin);

Vue.http.interceptors.push((request, next) => {
  NProgress.start();
  next(() => {
    NProgress.done();
  })
});

const router = new VueRouter({
  mode: 'history',
  routes,
  linkActiveClass: 'active'
});

window.App = new Vue({
  el: '#starting',
  delimiters: ['${', '}'],
  router,
  data: {
    currentNavbar: AdminNavbar
  },
  components: {
    'main-navbar': AdminNavbar
  }
});
