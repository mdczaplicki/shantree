from rest_framework import serializers

from main.models import Offer, Massage


class OfferSerializer(serializers.ModelSerializer):
    massage__name = serializers.PrimaryKeyRelatedField(source='massage.name',
                                                       required=False,
                                                       read_only=True)

    class Meta:
        model = Offer
        fields = '__all__'


class MassageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Massage
        fields = '__all__'
