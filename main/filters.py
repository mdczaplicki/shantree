import django_filters

from main.models import Massage, Offer


class MassageFilter(django_filters.FilterSet):
    name = django_filters.CharFilter(field_name='name', lookup_expr='icontains')

    class Meta:
        model = Massage
        fields = ['name']


class OfferFilter(django_filters.FilterSet):
    name = django_filters.CharFilter(field_name='name', lookup_expr='icontains')

    class Meta:
        model = Offer
        fields = ['name', 'visible']
