from rest_framework import viewsets
from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView

from main.filters import MassageFilter, OfferFilter
from main.models import Offer, Massage
from main.serializers import OfferSerializer, MassageSerializer


class OfferViewSet(viewsets.ModelViewSet):
    queryset = Offer.objects.all()
    serializer_class = OfferSerializer
    filterset_class = OfferFilter
    filterset_fields = ['name', 'visible']
    ordering_fields = ['name', 'id', 'visible', 'length', 'price', 'massage', 'massage__name']
    ordering = ['-id']
    permission_classes = [IsAuthenticated]


class MassageViewSet(viewsets.ModelViewSet):
    queryset = Massage.objects.all()
    serializer_class = MassageSerializer
    filterset_class = MassageFilter
    filterset_fields = ['name']
    ordering = ['-id']
    permission_classes = [IsAuthenticated]

    def paginate_queryset(self, queryset):
        if 'all' in self.request.query_params:
            return None
        return super().paginate_queryset(queryset)
