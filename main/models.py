from django.db import models


class Massage(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField()
    # image = models.ImageField()


class Offer(models.Model):
    name = models.CharField(max_length=255)
    length = models.IntegerField(null=True)
    price = models.IntegerField()
    visible = models.BooleanField()
    massage = models.ForeignKey(Massage, models.SET_NULL, null=True)
