from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import path, include, re_path
from django.views.generic import TemplateView
from rest_framework import routers

from main.views import OfferViewSet, MassageViewSet

router = routers.DefaultRouter()
router.register('offers', OfferViewSet, basename='offers')
router.register('massages', MassageViewSet, basename='massages')


urlpatterns = [
    path('api/', include(router.urls)),
    re_path('^.*$', TemplateView.as_view(template_name='index.html'))
]

urlpatterns += staticfiles_urlpatterns()
