import AdminPanel from './components/administrator_panel/admin-panel'
import Login from './components/administrator_panel/login'

const AdminMassages = () => import('./components/administrator_panel/massages');
const AdminOffers = () => import('./components/administrator_panel/offers');


const Dummy = { template: '<p>Test</p>'};


export const routes = [
  {
    path: '/admin',
    redirect: 'admin/masaze',
    component: AdminPanel,
    children: [
      {
        path: 'masaze',
        name: 'AdminMassages',
        component: AdminMassages
      },
      {
        path: 'oferty',
        name: 'AdminOffers',
        component: AdminOffers
      }
    ]
  },
  {
    path: '/logowanie',
    component: Login
  }
];
